<h1>Work Agreement for Group:</h1>
<h2>Group 7</h2>

<ul>
    <li>Member email: emil2512@edu.eal.dk</li>
    <li>Member email: alek2261@edu.eal.dk</li>
    <li>Member email: geno0010@edu.eal.dk</li>
    <li>Member email: anat0024@edu.eal.dk</li>
    <li>Member email:</li>
</ul>

<h2>Work agreement for Seven  Continents Project for full-time Consulting Group Members</h2>
<h3>You and your Co-Group Members agree to their regular hours of work, given in this Work Agreement.</h3>
<p>The Group members are hereafter referred to as “the members” in this work agreement.<br>
This Work Agreement states the terms and conditions that govern the contractual agreement between The Group Members who agrees to be bound by this Work Agreement.<br>
NOW, THEREFORE, In consideration of the mutual covenants and promises made by the parties hereto, the Group the Group Member (individually, each a “Party” and collectively, the “Parties”) covenant and agree as follows:</p>
<ul>
    <li><b>TERM.</b> The term of this Work Agreement shall commence on August 20, 2018. The Group Members agrees and acknowledges that, just as they have the right to terminate their participation in a group at any time for any reason, the Group has the same right, and may terminate their participation within the Group at any time for any reason. Either Party may terminate said employment with written notice to the other Party.</li>
    <li><b>DUTIES.</b> The Group Members accepts the terms and conditions set forth in this Work Agreement, and agrees to devote his full time and attention (reasonable periods of illness excepted) to the performance of his/hers duties under this Work Agreement. In general, the Group Member shall perform all the duties as described by the Project Plan developed in the Group</li>
</ul>

<h5>Group Members details:</h5>
<ul>
    <li>
        <b>Groupmember 1:</b>
        <ul>
            <li><b>First name: </b>Emil</li>
            <li><b>Surname: </b>Yung</li>
            <li><b>Position: </b></li>
        </ul>
    </li>
    <li>
        <b>Groupmember 2:</b>
        <ul>
            <li><b>First name: </b>Aleksandra</li>
            <li><b>Surname: </b>Slavova</li>
            <li><b>Position: </b>Team leader</li>
        </ul>
    </li>
    <li>
        <b>Groupmember 3:</b>
        <ul>
            <li><b>First name: </b>Geno</li>
            <li><b>Surname: </b>Mitev</li>
            <li><b>Position: </b>PM</li>
        </ul>
    </li>
    <li>
        <b>Groupmember 4:</b>
        <ul>
            <li><b>First name: </b>Anatoli</li>
            <li><b>Surname: </b>Penev</li>
            <li><b>Position: </b>TL</li>
        </ul>
    </li>
    <li>
        <b>Groupmember 5:</b>
        <ul>
            <li><b>First name: </b></li>
            <li><b>Surname: </b></li>
            <li><b>Position: </b></li>
        </ul>
    </li>
</ul>

<h3>Details of agreement:<h3>
<p>
<ul>
    <li>The members agree to minimum work the hours given in the chapter “Details of agreed working hours” and in The Project Plan.</li>
    <li>The members will work as according to the resource definition in the Project Plan for the specific Project (e.g. 100%, 75%, 50% etc.).</li>
    <li>The members will agree with the other team members, either orally, or in writing, when they are present in the Academy and when they work at home on the project.</li>
    <li>The members will agree what kind of media will be used for sharing information, e.g. Google docs etc., and what rules to follow for sharing information.</li>
    <li>The members will notify each other, as well as the Executive, in case of illness or other situations where a member is in a situation where he/she cannot attend the group activities in the Academy. This includes late appearance in the Academy (on-site) or when the group work off-site.</li>
    <li>
    In case a member is not fulfilling his/hers obligations it is up to the other members of the group to take actions as follows:
    <ul>
        <li>First occurrence of not fulfilling this working agreement, the member in question will be issued a written warning from the other group members.</li>
        <li>Second occurrence of not fulfilling this working agreement, the member in question will called in for a meeting with the other group members!</li>
        <li>Third occurrence of not fulfilling this working agreement, after having received first written warning and after having attended the meeting, the member in question will be eligible for being expelled from the group. This is a decision made by the group! In case the member is expelled, the group must then consult the Executive who will then contact the Student Counselor, and a decision will have to be made regarding the future activities of the expelled member.</li>
    </ul>
    </li>
</ul>
</p>
<p><b>Important Notice:</b> Being expelled from the group, at any time during the project, means the member cannot participate in the project any longer, as it is a 100% Team effort. The result can be that the expelled student will be barred from the Technology exam and Internship. This decision is made in cooperation with the Student Counselor. </p>
<h3>Details of agreed working hours:</h3>
<h4>It is agreed that the ordinary hours of work of the Students in this group will be varied to the arrangement specified below:</h4>
<p>Effective from (date):</p>
<p>Until (specify end date or ‘ongoing’):</p>
<table>
  <tr>
    <th>Day</th>
    <th>Start time (e.g. 8:30am)</th>
    <th>Break begins (e.g. 12:30pm)</th>
    <th>Return to work (e.g. 1:30pm)</th>
    <th>Finish time (e.g. 5pm)</th>
    <th>Other times/ Breaks</th>
    <th>Total (hours minus unpaid breaks)</th>
  </tr>
  <tr>
    <td>Monday</td>
    <td>08:30</td>
    <td>11:30</td>
    <td>12:15</td>
    <td>16:00</td>
    <td>0:30</td>
    <td>6:15</td>
  </tr>
  <tr>
    <td>Tuesday</td>
    <td>08:30</td>
    <td>11:30</td>
    <td>12:15</td>
    <td>16:00</td>
    <td>0:30</td>
    <td>6:15</td>
  </tr>
  <tr>
    <td>Wednesday</td>
    <td>08:30</td>
    <td>11:30</td>
    <td>12:15</td>
    <td>16:00</td>
    <td>0:30</td>
    <td>6:15</td>
  </tr>
  <tr>
    <td>Thursday</td>
  </tr>
  <tr>
    <td>Friday</td>
  </tr>
  <tr>
    <td>Saturday</td>
  </tr>
  <tr>
    <td>Sunday</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td><b>Total Hrs:</b></td>
    <td>21:45</td>
  </tr>
</table>