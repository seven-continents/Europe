from jnpr.junos import Device
import sys, pickle

london = '2001:0:1:4:20c:29ff:febd:5975'
paris = '2001:0:1:4:20c:29ff:fea8:f947'
copenhagen = '2001:0:1:4:20c:29ff:fe67:4d00'
firewall = '2001:0:1:4:20c:29ff:fede:8ad3'

HOSTIP = [paris,london,firewall,copenhagen]

def main():
    for ip in HOSTIP:
        DEV = Device(ip, user='root', password='Juniper1')
        connect_to_device(DEV, ip)
        show(DEV, ip)
        DEV.close()

def connect_to_device(DEV, ip):
    try:
        print ('opning connection to ', ip)
        DEV.open()
    except Exception as somethingIsWrong:
        print ('Unable to connect to host:', somethingIsWrong)
        sys.exit(1)

def show(DEV, ip):
        show = {DEV.cli('show configuration', warning=False)}
        save(show, ip)

def save(show, ip):
        if ip == '2001:0:1:4:20c:29ff:febd:5975':
            ip = 'london'
        elif ip == '2001:0:1:4:20c:29ff:fea8:f947':
            ip = 'paris'
        elif ip == '2001:0:1:4:20c:29ff:fe67:4d00':
            ip = 'copenhagen'
        else:
            ip = 'firewall'
        outfile = open(ip+'.md','wb')
        pickle.dump(show, outfile)
        outfile.close()

main()
