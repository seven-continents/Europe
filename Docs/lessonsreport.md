**LESSONS REPORT**

| **Project:**  |               |
|---------------|---            |
| Release:      | 7.12.2018    |
| Date:         | 7.12.2018    |
|               |               |
| **PRINCE2**   |               |
|               |               |
| Author:       |Aleksandra, Emil, Geno, Anatoli|
| Owner:        |  Peter        |
| Client:       | "The Company" |
| Document Ref: |               |
| Version No:   |   1           |

1 Lessons Report History
========================

1.1 Document Location
---------------------

This document is only valid on the day it was printed.

The source of the document will be found at this location – https://gitlab.com/seven-continents/Europe/blob/master/Docs/lessonsreport.md

1.2 Revision History
--------------------

**Date of this revision:**
7.12.2018

1.3 Approvals
-------------

This document requires the following approvals.

Signed approval forms should be filed appropriately in the project filing
system.

| **Name** | **Signature** | **Title** | **Date of Issue** | **Version** |
|----------|---------------|-----------|-------------------|-------------|
|   Peter  |               | Owner     |     7.12.2018     |    1        |

1.4 Distribution
----------------

This document has been distributed to:

| **Name** | **Title** | **Date of Issue** | **Version** |
|----------|-----------|-------------------|-------------|
|  Peter   |   Owner   |    14.12.2018     |    1        |


<br>2 Executive Summary
-----------------------
In this document you will see how we managed our team, what effort we needed for the project and what lessons we learned through the project. The project is about
building a international netwrok for a client, e.g "The Company" and we had build our netwrok on an ESXi. 


3 Scope of the Report
---------------------

The Project will be developed, designed and implemented of Consulting-Teams from
University College Little belt, 3- Semester Students, from IT Technology Network Education.
The aim is to build an infrastructure for the company, which will distribute a
number of Data Centers/Network Hub/PE Sites geographically (in Seven Continents)
and interconnect them with a Service Provider MPLS/RSVP Backbone.
The ConsultingTeams will be responsible for the design and Implementation of the SP Network.


4 Project Review
----------------

-   Project management method:
    - In our team we have a Project manager that rotates every two weeks. The PM is responsible for the managment in the group and
    sometimes assign people to takst in the GitLab project.

-   Any specialist methods used:
    - We decided to do group meeting every week so that we can discuss what we have done, what we have to do next and ask questions if we have some.

-   Project strategies - We had specified different documents in our project that help us in the process:
    - Tasks managment - Here is link to our Task distribution document: https://gitlab.com/seven-continents/Europe/blob/master/Docs/TaskDistribution.md
    - Risk managment - Here is a link to the Risk assesment document: https://gitlab.com/seven-continents/Europe/blob/master/Docs/Risk_assessment.md
    - Communication managment - Here is a lnk to our Communication plan: https://gitlab.com/seven-continents/Europe/blob/master/Docs/CommunicationPlan.md

-   Project controls:
    - We have a set of rules specified in our Work agreement: https://gitlab.com/seven-continents/Europe/blob/master/Docs/Work%20agreement.md

-   Abnormal events causing deviations: 
    - We had one connectet to one of the risks in our Risk assesment. We lost one member of our team because of the re examination and then he returned back after a while so we 
    lost time and human resources. 

5 Measurements Review
---------------------

-   We needed a lot of effort in order to manage our team and structure the work between us.
- After creating the documents such as Work agreement, Communication plan and Task distribution
it was already easy to follow the tasks that we have and complete the project successfully.


6 Significant Lessons
---------------------

-   Calculatin hardware is hard.

-   That slows the working process.

-   There weren't early indicators for this. We found out that is dificult when we tried to do the task.

-   The recomondation is to put more time on it.

-   It wasn't identified in the Risk assessment. 
