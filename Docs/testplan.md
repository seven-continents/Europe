**Test plan**

**Europe**

| **Name**                         | **Date**         | **Version**       |
|----------------------------------|------------------|-------------------|
| **Anatoli**                      | **27.11**        | **Initial draft** |
| **Anatoli**                      | **28.11**        | **v 0.1**         |
| **Anatoli**                      | **28.11**        | **v 0.2**         |
| **Anatoli**                      | **28.11**        | **v 0.3**         |
| **Anatoli**                      | **28.11**        | **v 0.4**         |
| **Anatoli**                      | **03.12**        | **Final version** |



|#| **Name of test**                 |**Expected result**| **Pass/Fail**     | **Commentary**  |
|-|----------------------------------|-------------------|-------------------|-----------------|
|1| **Ping to Copenhagen**           |  *Ping received*  |     *Pass*        |                 |
|2| **Ping to Paris**                |  *Ping received*  |     *Pass*        |                 |
|3| **Ping to London**               |  *Ping received*  |     *Pass*        |                 |
|4| **Backup script working**        |*Successful backup*|     *Pass*        |                 |
|5| **Ping webserver in London**     |  *Ping received*  |     *Pass*        |                 |
|6| **Access to website in London**  | *Load successful* |     *Pass*        |                 |
|7| **Ping fileserver in London**    |  *Ping received*  |     *Pass*        |                 |
|8| **Access fileserver in London**  | *Load successful* |     *Pass*        |                 |
|9| **Ping to LibreNMS in CPH**      |  *Ping received*  |     *Pass*        |                 |
|10| **Access to LibreNMS in CPH**   |*Access successful*|     *Pass*        |                 |
|11| **Ping to Jump-Host**           |  *Ping received*  |     *Pass*        |                 |
|12| **Ping webserver in DMZ**       |  *Ping received*  |     *Pass*        |                 |
|13| **Access to webserver in DMZ**  | *Load successful* |     *Pass*        |                 |
|14| **Ping to PE router**           |  *Ping received*  |     *Pass*        |                 |
