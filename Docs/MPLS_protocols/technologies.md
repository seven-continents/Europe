<table>
    <tr>
        <th>Protocols/technologies</th>
        <th>Implemented</th>
        <th>Skiped</th>
    </tr>
    <tr>
        <td>Resource Reservation Protocol (RSVP)</td>
        <td>X</td>
        <td></td>
    </tr>
    <tr>
        <td>Label Distribution Protocol (LDP)</td>
        <td></td>
        <td>X</td>
    </tr>
    <tr>
        <td>iBGP</td>
        <td>X</td>
        <td></td>
    </tr>
    <tr>
        <td>IS - IS</td>
        <td>X</td>
        <td></td>
    </tr>
    <tr>
        <td>Traffic Engineering (TE)</td>
        <td>X</td>
        <td></td>
    </tr>
    <tr>
        <td>Explicit Route Objects (ERO)</td>
        <td></td>
        <td>Not implementet. It was decided that there was no need for ERO, because the MPLS network is set up in a POC environment. There is no performance issues </td>
    </tr>
    <tr>
        <td>Class of Service (CoS)</td>
        <td></td>
        <td>Not implemented since we don't got any perfermance issues in the MPLS backbone so no reason to filter packes </td>
    </tr>
    <tr>
        <td>RSVP Link Protection</td>
        <td></td>
        <td>Not implementet. Because the MPLS network is connected with several links in LAG. The links are not expected to go down.</td>
    </tr>
    <tr>
        <td>RSVP Node Protection</td>
        <td></td>
        <td>Node protection is not used in this network as at this time there was limited resources for such implementation.</td>
    </tr>
    <tr>
        <td>Fast Reroute</td>
        <td></td>
        <td>fast reroute was not a service being used in the core network. The possibility of using this technology is implemented (RSVP).</td>
    </tr>
    <tr>
        <td>Backup LSP</td>
        <td>The infrastructure for backup links was implemented, but the configuration of backup labeled switched paths for the existing ones were not. It is important to note that the possibility in the future for such a configuration is much easier due to RSVP – TE being used instead of the IGP dependent LDP.</td>
        <td></td>
    </tr>
    <tr>
        <td>Virtual Route Forwarding instance (VRF)</td>
        <td></td>
        <td>VRF was not a technology used in the MPLS network due to the fact, that there were no instances of overlapping IP addresses used between the customer networks.</td>
    </tr>
    <tr>
        <td>IPv6 in MPLS Core</td>
        <td></td>
        <td>IPv6 was not implemented for the sole purpose for not having to run dual stack and the possibility of configuring 6PE.</td>
    </tr>
    <tr>
        <td>eBGP PE to CE Routers</td>
        <td>X</td>
        <td></td>
    </tr>
</table>