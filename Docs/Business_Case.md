**PRINCE2™- Business Case**

| **Project Name:**    | Seven Continents                |                        |             |
|----------------------|---------------------------------|------------------------|-------------|
| **Date:**            | 20.08.2018                      | **Release:7.12.2018** | Draft/Final |
| **Author:**          | Aleksandra, Geno, Anatoli, Emil |                        |             |
| **Owner:**           | Peter                           |                        |             |
| **Client:**          | 'The Company'                   |                        |             |


**Distribution**

This document has been distributed to:

| **Name** | **Title** | **Date of Issue** | **Version** |
|----------|-----------|-------------------|-------------|
| Peter    | Owner     |                   |      1      |
|          |           |                   |             |

**Overview**

| **Purpose**  | A Business Case is used to document the justification for the undertaking of a project, based on the estimated costs (of development, implementation and incremental ongoing operations and maintenance costs) against the anticipated benefits to be gained and offset by any associated risks. The outline Business Case is developed in the Starting up a Project process and refined by the Initiating a Project process. The Directing a Project process covers the approval and re-affirmation of the Business Case. The Business Case is used by the Controlling a Stage process when assessing impacts of issues and risks. It is reviewed and updated at the end of each management stage by the Managing a Stage Boundary process, and at the end of the project by the Closing a Project process. |
|--------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Contents** | *The Business Case should cover the following topics.*  Executive Summary, Reasons, Business Options, Expected Benefits, Expected Dis-benefits, Timescale, Costs, Major Risks, Risk Management                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |


# Executive Briefing

### Decision to be Taken

-   If we want to do it in, the city that is stated right now (Copenhagen, Londag and Paris). Or if we move some of them.
-   What routers we need.

# Introduction

### Scope

The Project will be developed, designed and implemented of Consulting-Teams from
University College Little belt, 3- Semester Students, from IT Technology Network Education.
The aim is to build an infrastructure for the company, which will distribute a
number of Data Centers/Network Hub/PE Sites geographically (in Seven Continents)
and interconnect them with a Service Provider MPLS/RSVP Backbone. 
The ConsultingTeams will be responsible for the design and Implementation of the SP Network.

### Reasons

The reason of this Seven continents project is to give us an opportunity for
work on real project for a customer. It will teach us how to deal with real
problems and will make us responsible team workers.

### Business Options

We will work with a bit of changes here and there, to make everything a bit smarter. And work every monday till wednesday from 8:15 till 16:00.

# Analysis

### Expected Benefits

-   worldwide connection
-   data centers all over the world
-   privacy 
-   faster communication

### Expected Dis-benefits

- more equipment 
- higher operating cost
- reduction in productivity during the time taken for users to learn to use a new software product

### Timescale

This project starts on Monday, August 20, 2018 which is week 34
and must be done by Friday, December 14, 2018 week 50.
- And here is a link to our MS Project: https://gitlab.com/seven-continents/Europe/blob/master/Docs/The_projekt.mpp

### Costs

-   Estimated work cost: 817 500 kr.
-   Estimated risk work cost: 163 500 kr.

-   Equipment:<br>
    - 3 x MX204 price 194 890,5kr <b>Total</b> 584 671,5kr <br>
    - 1 x SRX5600 price  313 950kr <b>Total</b> 313 950kr <br>  
    - 2 x SRX5600PWR2400DCS price 24 150kr <b>Total</b> 48 300kr <br>
    - 9% per year for Operating cost <br>

### Major Risks

- One member of the team has a pending exam. This creates a risk for the group losing a member if exam is not passed.
- Running out of money.
- Cracked password
- Cross site scripting
- The data could be inappropriately extracted
- Web server and application server may run unnecessary services
- Disaster recivery plan has not been established 
- Fire in the server room 

### Risk managment ###
In case a problem occurs our plan is to first assess the situation and the consecuandance that will come. Then we will discuss the solutions that we have and pick the best one
Our task is to solve the problem or prevent the risk before it becomes too comlpicated and with a big influence to the Project.