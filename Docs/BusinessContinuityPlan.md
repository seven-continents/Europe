### 1. Initial data ###
----------------

- Responsible people: 
    - Aleksandra - alek2261@edu.eal.dk
    - Emil - emilerx13@edu.eal.dk
    - Geno - geno0010@edu.eal.dk
    - Anatoli - tolipenev@edu.eal.dk

### 2. Revision Managment ###
---------------------
- We have a project manager.
- We change the PM every two weeks. 

### 3. Purpose and scope ###
--------------------
In case of disaster or problem in the network after the project is done our actions should be structured and organised in order to limit the damages that will grow up whit the time passing. 
That's why we need this plan and we need to follow it in those kind of situations. 

### 4. How to use the plan ###
----------------------
- When the plan should be activated?
    - This plan should be used in any situation of disaster after the poject is done.
- Time frames
    - It should be activated as soon as possible.
- Who declares a disaster?
    - When someone find somenting that's not good or a disaster happens, he/she have to declare it.
- Who should be contacted in this situations?
    - In this kind of situation the person declaring the disaster should contact:
        - The other team members
        - or if the problem is bigger he/she should ask for help the teacher.

### 5. Provide policy information ###
------------------------------
Group members have signed and agreed to a work agreement document. It is specified there what problems can arrise and how to solve them. 
Work hours and how to deal with illness and absence are also documented. 


### 6. Plan review and maintenance ###
-------------------------------
The plan should be reviewed and if needed updated every two weeks from the new PM. 

### 7. Risk assessment ###
----------------------
- When a problem or a risk occurs the team should
    - Evaluate the damage.
    - Discuss and control the situation.
    - Follow the specified protocols. 
    - Take actions.

### 8. Business continuity  ###
---------------------
Business continuity refers to maintaining business functions or quickly resuming them in the event of a major disruption, whether caused by a fire, 
flood or malicious attack by cybercriminals. A business continuity plan outlines procedures and instructions an organization must follow in the face of such 
disasters; it covers business processes, assets, human resources, business partners and more.
