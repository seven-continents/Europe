**Introduction**
================

The purpose of the risk assessment was to identify threats and vulnerabilities
related to the Seven continents project – Group 1 Europe (known as “project”).
The risk assessment will be utilized to identify risk mitigation plans related
to the project. The project was identified as a potential high-risk system in
the Department’s annual enterprise risk assessment


**Risk Assessment Approach**
============================

Participants:

| **Participant**    | **Role**        |
|--------------------|-----------------|
| Emil Yung          | Team Member     |
| Geno Mitev         | Project Manager |
| Aleksandra Slavova | Team Leader     |
| Anatoli Penev      | Team Member     |


**Risk MODEL**

In determining risk associated with the project, we utilized the following model
for classifying risk:

Risk = Threat likelihood x Magnitude of the Impact
--------------------------------------------------


**Threat Likelihood**

| **Likelihood** | **Definition**                                                                                                                                                   |
|----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| High           | The threat-source is highly motivated and sufficiently capable, and controls to prevent the vulnerability from being exercised are ineffective                   |
| Medium         | The threat-source is motivated and capable, but controls are in place that may impede successful exercise of the vulnerability.                                  |
| Low            | The threat-source lacks motivation or capability, or controls are in place to prevent, or at least significantly impede, the vulnerability from being exercised. |


**Magnitude of Impact**

| **Impact**      | **Definition**                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **High (100)**  | The loss of confidentiality, integrity, or availability could be expected to have a severe or catastrophic adverse effect on organizational operations, organizational assets, or individuals. Examples: • A severe degradation in or loss of mission capability to an extent and duration that the organization is not able to perform one or more of its primary functions • Major damage to organizational assets • Major financial loss                                      |
| **Medium (50)** | The loss of confidentiality, integrity, or availability could be expected to have a serious adverse effect on organizational operations, organizational assets, or individuals. Examples: • Significant damage to organizational assets • Significant financial loss • Significant harm to individuals that does not involve loss of life or serious life-threatening injuries.                                                                                                  |
| **Low (10)**    | The loss of confidentiality, integrity, or availability could be expected to have a limited adverse effect on organizational operations, organizational assets, or individuals. Examples: • Degradation in mission capability to an extent and duration that the organization is able to perform its primary functions, but the effectiveness of the functions is noticeably reduced • Minor damage to organizational assets • Minor financial loss • Minor harm to individuals. |


**Risks that might occur**

1.  A member of the team has a pending exam. This creates a risk for the
    group losing a member if he doesn’t pass it.

2.  The server room is a fire hazard place and if something is to happen, work
    on the project might be interrupted.

3.  Cross-site scripting - The web application can be used as a mechanism to
    transport an attack to an end user's browser. A successful attack can
    disclose the end user’s session token, attack the local machine, or spoof
    content to fool the user.

4.  SQL injection Information from web requests is not validated before being
    used by a web application. Attackers can use these flaws to attack backend
    components through a web application.

5.  Password strength - Passwords used by the web application are
    inappropriately formulated. Attackers could guess the password of a user to
    gain access to the system.

6.  Unnecessary services - The web server and application server have
    unnecessary services running such as telnet, SNMP and anonymous ftp

7.  Disaster recovery - There are no procedures to ensure the ongoing operation
    of the system in event of a significant business interruption or disaster v

8.  Lack of documentation - System specifications, design and operating
    processes are not documented.

9.  Integrity checks - The system does not perform enough integrity checks on
    data input into the system.


**Risk Assessment Results**

| \# | Observation                                                                                                   | Threat-source                       | Existing controls                                        | Likelihood | Impact | Risk Rating | Recommended controls                                                                                                                                                                                                                     |
|----|---------------------------------------------------------------------------------------------------------------|-------------------------------------|----------------------------------------------------------|------------|--------|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | User system passwords can be guessed or cracked                                                               | Hackers / Password effectiveness    | Passwords must be alphanumeric and at least 5 characters | Medium     | Medium | Medium      | Use of SSH keys instead of password.                                                                                                                                                                                                     |
| 2  | Cross site scripting                                                                                          | Hackers / Cross-site scripting      | None                                                     | Medium     | Medium | Medium      | Validation of all headers, cookies, query strings, form fields, and hidden fields (i.e., all parameters) against a rigorous specification of what should be allowed                                                                      |
| 3  | Data could be inappropriately extracted/modified from DMV database by entering SQL commands into input fields | Hackers / SQL Injection             | Limited validation checks on inputs                      | High       | Medium | Medium      | Ensure that all parameters are validated before they are used. A centralized component or library is likely to be the most effective, as the code performing the checking should all be in one place. Each parameter should be checked ! |
| 4  | Web server and application server running unnecessary services                                                | All / Unnecessary Services          | None                                                     | Medium     | Medium | Medium      | Reconfigure systems to remove unnecessary services                                                                                                                                                                                       |
| 5  | Disaster recovery plan has not been established                                                               | Environment / Disaster Recovery     | Weekly backup only                                       | Medium     | High   | Medium      | Develop and test a disaster recovery plan                                                                                                                                                                                                |
| 6  | Team assessment                                                                                               | Re exam                             | Self-dependent                                           | Low        | High   | Medium      | Self assessment                                                                                                                                                                                                                          |
| 7  | Fire in the server room                                                                                       | Fire                                | Smokes detectors and water sprinklers                    | Low        | High   | High        | Room should be cooled with air conditioner for maintaining low temperatures on devices.                                                                                                                                                  |
| 8  | Lack of documentation                                                                                         |                                     | Writing everything down                                  | Low        | High   | Medium      | Re-check all documents to confirm all are up-to-date.                                                                                                                                                                                    |
