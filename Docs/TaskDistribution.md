### Tasks distribution rules: ###

- The tasks should be distributed evenly between the team members.
- The tasks should be assigned depending on the skills of the team member.
- When a group member wants to take a task they can assign themselves to it.
- If a group member can't complete the task he should discuss it with the team members.