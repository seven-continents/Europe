### 1. Weekly check-ins: ###
----------------------------

- Weekly presentations

    - This presentation should include the proggress of the group in the project.
    - What's the purpose
    - The class is the audience

- We can also communicate with Peter through GitLab, in person or by sending emails.


### 2. Communication between the team ###
-------------------------------------------

- In GitLab
    - Sharing files
    - Deviding and distributing tasks
    - Following the proggress of the group

- Facebook Messanger - communication between the team for stuff such as:
    - Absence
    - If someone is late
    - Questions

- Weekly group meetings
    - In order to discuss what we have done and what we have to do next.