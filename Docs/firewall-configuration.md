![](Diagrams/firewall-zones.jpg)

<h2>Security zones:</h2>

<b>Trust</b>- This is our internal network and it can only be accessed by our
management network.

<b>Untrust</b> – This is the the zone with all the continent networks we are connect
to.

<b>DMZ</b>- This is the only zone that is accessible from the continents

<b>MGMT</b>- This is our management network which we use to access all our network
devices.

<h2>Security policies:</h2>

From zone <b>trust</b> to zone <b>DMZ</b> – We allow every application

From Zone <b>trust</b> to zone <b>untrust</b> – We allow every application

From zone <b>DMZ</b> to zone <b>trust</b> – We allow every application

From zone <b>DMZ</b> to zone <b>untrust</b> – We allow every application

From zone <b>untrust</b> to zone <b>DMZ</b> – We allow every application

From zone <b>untrust</b> to zone <b>trust</b> – We allow only ping
