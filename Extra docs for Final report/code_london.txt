<configuration-text>
## Last changed: 2018-12-05 10:15:01 UTC
version 17.2R1.13;
system {
    host-name London;
    root-authentication {
        encrypted-password "$6$17jAjJ.w$OR.SB8Xvgpgud84e6nC6Y0aBdBB6WKWtepvDxPksv.HYp/J.4WkUM3ax795j0uYtPvA4yRzTDC08.ngpcUq6D.";
        ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDH9Lud0wCS0QdjjIcqNugw7sm7PIUzdSucSDoXG8IHWSjm7Wud6XcYytJ/hmD4lU8gLAS73O7FB3xabnnuOAj4cqj7B2g0JrQsoD4jPWVC8wWPukZAor5AmZnHCtGuenb8erkhm/rAvc15pENVC9tN6DMcDK+qB0S0m2akiezBaLiN3lqVHQIjhaLY0Vof+TmXBl8023ch8mMaQKLnkWQ37MP+BGULD97DGPYLP6a/OPzI/R7QTzsYZ9LAoLI07cLsIJ6rj4AWEpykoWZgS0Te7ipYu+zL19IRl9lneJWRv9dBY0/b7dy5kWJQIvDQ7IUF2ah+bryJqURMkGlBY6GDRdeeTu9xVqUvT5jPfkVXK0LyCuSSr45yYi/h1S6x+4ANUdTmYvRxKt7PCOVvWqC44Lq8qscgI+fQigamwgLK04UgZNhtxehS4yl+z7ZQkX2KbVzPumF9+J3KEp4oetadqim4E47lHopvOP2ZSQ9rJONPbHXA4jwfkRu2F/X+WpsJX470Qo+rkPZV14Dhp02ktA9uJt+uKrEa1pRmr2/WixF4JA/ZCnwMMFknZD+VVVRqTGHeJ6Obvc0Eqffc0yPQMl7JZCvwuxkWsSG2fUeAb0moWL794NzHrUFzS4/v0YTCoH/QGWc3crIFlZEJUv0ETrv9LZAFH2nGDkQNKlSwiw== emiler-x-13@hotmail.com";
        ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC7cGfp77zMeq4YC5+D7URLM4ddLGUVH2DpR5ZEiVxoOdxvbwK3tBpBnYZGkOfHQkEbr/nuWkhOLT/AoWAHEnWdZo1+56KtLb63M+d9PbHIRpHYIHjvEAfiCctdgQCZsPCWOngsz64g69Bu/9QqQdlNwRd1u/0NK7agj3+EDuRF/RtFsH94ALrPrAGEBVz3sqbpbvmFJH2qWmq+U1YY3LMJpZPJqteqtZh3rYmZyX4qbRyDfcs7vVqrGytr6zG8dnAVTsgVDsTMpYOH5RYStbrnx57MAewS5G6PobMvExHNL0LA1e/EscrjUaPLOfF/jONfF9pgfON63Ni4vjGUwbsMRFthEGLqapefblniWhG5Omvk82fXHq4Mcm4v7a+0rG3wJQtixc0E7O/GdrxSKtbwlwgQIOQ91g8X0Cn4IgaacDLhGQM+MFYG4rJ8oBe/GcJj8JZpUFCvhZyA49p5WHEzMwtx96b0JNK+FQV+ziuJzecrmpLP7Pj2KK2s1dGyh8s6R/Kj4A96+w2J96SxMT6YUJ4IkxZ4k0lKzvfQZWXgimmUrz2UXb6ilz9X7liE3/CrD+EWG+Ih1d66h3U3JoBKqWt0/JlDOX/Kiwoje4njeJzJDNm/OzxQUMphOk7H1I4zPghWW6TLkPKrHOele6MTSHyEva7PLzXjmcMgf3l0uw== tolis.penev@gmail.com";
        ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCWPMxAz46ydo3/L0rq1ygZAS/NO5E6oKoZjJ+CW34TG5UcU69rwUab8hh0Ba4O28H0B/p70+Ur96WKSmd8jhLmCkjsfHUIZ55gVX60dvP8Ylx+R677Wmcby7tMWh7w7ok8K6VEClyUVCNN5KuYBT6J530mLqrJXeCV+0Dtv6g5iGSL2R/wBgdVkFXWMayXT+sFqP3DtX03l5ypFE1xNYDFFIVoCGlrQv9rhPqDmBstha4CgRjVVotD8fUNNt28s6hlzisqwtHIEqLjpESL6GJ64AOhW7L6v6VqShQFtFbArrXwBJb5+GoEpFnb/8u8o8LnBoMxZkOt7b9WLdlox+hh Alexandra Slavova@DESKTOP-0M8A2GS";
        ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDca8iERz/LL6XKxhz4kAOmOOjlkKOT0NUQ8pTYRZJFRpcHk6or+r3h2P3GQyWMVN3fN66GF29ENqa5LpcTuSdSkerrtLN29GWYZofxEvR1xSzDpaqXT5cDt1dwRsnln1hCa1C3cjqeicNBenN3h6tOO+EQ05q7zG/8AidbQp67HI8B+gUtaal37Ly5gmYkI7gT++gal8qK+Jz5KZUcC0x9El1sgJnW8+Uav54Ex04Afjw3u88M7Rm0yrYw5Kh9LxOPDx/wx85Ud2hwxg1AUnKv7BKvpHdebMjF8Kwvpz9h2XCpfhnF/UgQ05mBH6NQaAKQFbJwoqUvO2iFxUYGwphd XXX@HP13-GenoMitev";
        ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDK3yufe+SyBHAD6zytXWZ22lXGgIvZoyLxX3ohxaHaibWLZJc63LBtOde6/QO3IgT1pUnyEgFuQNkpwU0BcGSuK16pizzICBo4uIK9MNo9gBXAaA1D9Wpq0+PwHZVrqZI0MUNiJDpijNBh8bjYn0R8WpbzOsznA5LMrKv1m8bD4Ug1L6u2Nbe2gIAEu22PyvGOaOpqhyt/b5n2drK11I3WaXJ6AUVuKTa/oaceNzNAiddZkC/cwEwQ2xuy64MnbEgo9EWWUzQEanl+VIKubOX+MEW2RpxIf9TGPKuPYo09awl0Qy0XKH604e4lM40uoNO9ReLqmQEJKaFt8v1M4dXP debian@debian";
    }
    services {
        ssh {
            root-login allow;
        }
        netconf {
            ssh;
        }
    }
    syslog {
        user * {
            any emergency;
        }
        file messages {
            any notice;
            authorization info;
        }
        file interactive-commands {
            interactive-commands any;
        }
    }
    processes {
        dhcp-service {
            traceoptions {
                file dhcp_logfile size 10m;
                level all;
                flag all;
            }
        }
    }
}
interfaces {
    ge-0/0/0 {
        unit 0 {
            family inet6 {
                address 2001:0:1:5::/64 {
                    eui-64;
                }
            }
        }
    }
    ge-0/0/1 {
        unit 0 {
            family inet6 {
                address 2001:0:1:4::/64 {
                    eui-64;
                }
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet6 {
                address 2001:0:1:7::/64 {
                    eui-64;
                }
            }
        }
    }
    ge-0/0/3 {
        unit 0 {
            family inet6 {
                address 2001:0:1:2::/64 {
                    eui-64;
                }
            }
        }
    }
    fxp0 {
        unit 0 {
            family inet {
                dhcp {
                    vendor-id Juniper-vmx;
                }
            }
        }
    }
    lo0 {
        unit 0 {
            family inet6 {
                address 2001::30/128;
            }
        }
    }
}
snmp {
    name vMX-London;
    description Librenms;
    location London;
    contact europe;
    community public {
        authorization read-only;
    }
}
routing-options {
    router-id 3.3.3.3;
    autonomous-system 65001;
}
protocols {
    router-advertisement {
        interface ge-0/0/0.0 {
            dns-server-address 2001:0:2:20::4;
            prefix 2001:0:1:5::/64;
        }
    }
    ospf3 {
        area 0.0.0.0 {
            interface ge-0/0/2.0;
            interface ge-0/0/3.0;
            interface lo0.0 {
                passive;
            }
            interface ge-0/0/0.0;
        }
    }
    lldp {
        interface ge-0/0/0;
        interface ge-0/0/2;
        interface ge-0/0/3;
    }
}
</configuration-text>
