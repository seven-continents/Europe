from jnpr.junos import Device
from lxml import etree

LONDON = '2001:0:1:4:20c:29ff:febd:5975'
PARIS = '2001:0:1:4:20c:29ff:fea8:f947'
COPENHAGEN = '2001:0:1:4:20c:29ff:fe67:4d00'
FIREWALL = '2001:0:1:4:20c:29ff:fede:8ad3'

HOSTIP = [PARIS,LONDON,COPENHAGEN, FIREWALL]

def connect():
    for ip in HOSTIP:
        with Device(ip, user='root', password='Juniper1') as dev:
            get_stuff(dev, ip)

def get_stuff(dev, ip):
    data = dev.rpc.get_config(options={'format':'text'})
    save(data, ip)

def save(data, ip):
    if ip == LONDON:
        ip = 'london'
    elif ip == PARIS:
        ip = 'paris'
    elif ip == COPENHAGEN:
        ip = 'copenhagen'
    else:
        ip = 'firewall'
    f = open(ip+'.txt', 'w')
    f.write(etree.tostring(data, encoding='unicode'))
    f.close()

connect()
